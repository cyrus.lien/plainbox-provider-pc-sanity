plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-rapl
requires: cpuinfo.type == 'GenuineIntel'
command:
 if test -d /sys/class/powercap/intel-rapl; then
     exit 0
 else
     echo "Is CONFIG_INTEL_RAPL configured?"
     echo "List node under /sys/class/powercap"
     ls /sys/class/powercap
     echo "The info of powercap driver:"
     echo "https://www.kernel.org/doc/html/latest/power/powercap/powercap.html"
     echo "The info of Intel thermal_daemon:"
     echo "https://github.com/intel/thermal_daemon/blob/master/README.txt"
     exit 1
 fi
_summary: Check if Intel RAPL power capping driver is installed
_description:
 Check if Intel RAPL power capping  driver is installed

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-rapl-mmio
requires: cpuinfo.type == 'GenuineIntel'
depends: miscellanea/proc_thermal
command:
 if test -d /sys/class/powercap/intel-rapl-mmio; then
     exit 0
 else
     echo "Lack of MMIO interface for power capping."
     echo "It depends on proc_thermal, plesae check if processor thermal device exist"
     echo "and proc_thermal driver has probed properly."
     lspci -nnk
     echo "The info of powercap driver:"
     echo "https://www.kernel.org/doc/html/latest/power/powercap/powercap.html"
     echo "The info of Intel thermal_daemon:"
     echo "https://github.com/intel/thermal_daemon/blob/master/README.txt"
     exit 1
 fi
_summary: Check if Intel RAPL-mmio power capping driver is installed
_description:
 Intel RAPL (Running Average Power Limit) relies on proc_thermal
 driver to provide MMIO interface for power capping

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-p-state
requires: cpuinfo.type == 'GenuineIntel'
command:
 if test -d /sys/devices/system/cpu/intel_pstate; then
     exit 0
 else
     echo "Is CONFIG_X86_INTEL_PSTATE configured?"
     echo "List node under /sys/devices/system/cpu"
     ls /sys/devices/system/cpu
     echo "The intel_pstate driver is required by thermald."
     echo "The info of Intel thermal_daemon:"
     echo "https://github.com/intel/thermal_daemon/blob/master/README.txt"
     echo "The info of Intel pstate driver:"
     echo "https://www.kernel.org/doc/html/latest/admin-guide/pm/intel_pstate.html"
     exit 1
 fi
_summary: Check if Intel P State driver is installed
_description:
 Check if Intel P State driver is installed

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-powerclamp
requires: cpuinfo.type == 'GenuineIntel'
command:
 if grep -q 'intel_powerclamp' /sys/class/thermal/cooling_device*/type; then
     exit 0
 else
     echo "Is CONFIG_INTEL_POWERCLAMP configured?"
     echo "List node type under /sys/class/thermal/cooling_device*"
     grep . /sys/class/thermal/cooling_device*/type
     echo "The info of Intel powerclamp driver:"
     echo "https://www.kernel.org/doc/Documentation/thermal/intel_powerclamp.txt"
     exit 1
 fi
_summary: Check if Intel Power clamp driver is installed
_description:
 Check if Intel Power clamp driver is installed

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-cpu-thermal
requires: cpuinfo.type == 'GenuineIntel'
command:
 if grep -q 'B0D4\|B0DB\|TCPU' /sys/class/thermal/thermal_zone*/type; then
     exit 0
 elif grep -q 'coretemp' /sys/class/hwmon/hwmon*/name; then
     exit 0
 else
     echo "No valid sysfs node to create cpu thermal zone"
     lspci -nnk
     echo "node in thermal class"
     paste <(ls -d /sys/class/thermal/thermal_zone*) <(cat /sys/class/thermal/thermal_zone*/type) \
           <(cat /sys/class/thermal/thermal_zone*/temp) \
           | column -s $'\t' -t | sed 's/\(.\)..$/.\1°C/'
     echo "node in hwmon class"
     cat /sys/class/hwmon/hwmon*/name
     exit 1
 fi
_summary: Check if Intel cpu thermal zone registered
_description:
 Thermald checks if /sys/class/thermal/thermal_zone* contains type B0D4, B0DB, and TCPU.
 They are devices defined in BIOS and enumerated by int340x thermal driver.
 If those devices aren't defined in BIOS, thermald will capture coretemp under
 /sys/class/hwmon/hwmon* to create cpu thermal zone named cpu.

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel-x86-pkg-temp-thermal
requires: cpuinfo.type == 'GenuineIntel'
command:
 if grep -q 'x86_pkg_temp' /sys/class/thermal/thermal_zone*/type; then
     exit 0
 else
     echo "The system has no x86_pkg_thermal zone."
     echo "The info of Intel x86_pkg_thermal zone:"
     echo "https://www.kernel.org/doc/html/latest/driver-api/thermal/x86_pkg_temperature_thermal.html"
     exit 1
 fi
_summary: Check if X86 package temperature thermal driver is installed
_description:
 Check if X86 package temperature thermal driver is installed

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/valid-thermal-zone-trip-points
requires: cpuinfo.type == 'GenuineIntel'
command:
 fail=0
 for zone in /sys/class/thermal/thermal_zone*
 do
     tp_type=$(cat "$zone"/type)
     if [ "${tp_type:0:7}" = "iwlwifi" ]; then
         continue
     elif [ "${tp_type:0:15}" = "INT3400 Thermal" ]; then
         continue
     fi
     # shellcheck disable=SC2013
     for tp_temp in $(cat "$zone"/trip_point_*_temp)
     do
         # shellcheck disable=SC2086
         if [ $tp_temp -lt 0 ]; then
             echo "$tp_type contains invalid trip point"
             tail "$zone"/trip_point_*_temp
             echo "Please consult ODM to see if it is expected."
             fail=1
             break
         fi
     done
 done
 if [ $fail -eq 1 ]; then
         echo "For more detail, please refer to https://01.org/intel%C2%AE-dynamic-platform-and-thermal-framework-dptf-chromium-os/overview"
         exit 1
 fi
_summary: Check if all trip points are valid
_description:
 Check if all trip points are valid, but ignore iwlwifi registered
 trip points.

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/proc_thermal
requires:
  cpuinfo.type == 'GenuineIntel'
  device.path in ['/devices/platform/INTC1040:00','/devices/platform/INT3400:00']
command:
 set -x
 report_folder="$HOME/submission-report-$(date +%Y%m%d%H%M)"
 mkdir -p "$report_folder"
 if lspci -nnk  | grep driver | grep proc_thermal;then
        true;
 else
        journalctl -k > "$report_folder"/dmesg.log
        lspci -nnk > "$report_folder"/dmesg.log
        lsmod > "$report_folder"/lsmod.log
        false;
        echo "proc_thermal driver is not loaded."
        echo "It could caused by lake of ID in kernel like LP: #1874008."
        exit 1
 fi
_summary: Check if thermal driver be loaded.
_description:
 Check if thermal driver be loaded. Which also checked if the CPU id be recognized by kernel.
 Thermal zone TCPU will not be created without this driver which also impact thermald operation.

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/thermald
command:
 systemctl is-active --quiet thermald
 RETVAL=$?
 if [ $RETVAL -ne 0 ];
 then
     journalctl -b -u thermald > "$PLAINBOX_SESSION_SHARE"/journalctl.log
     echo "The log for thermald: $PLAINBOX_SESSION_SHARE/journalctl.log"
     grep -q "Unsupported cpu model" < "$PLAINBOX_SESSION_SHARE"/journalctl.log && \
     echo "Debugging Tips:"
     echo "Please check the upstream code(https://github.com/intel/thermal_daemon/blob/master/src/thd_engine.cpp) for the target id in id_table[] " && \
     echo "If the ID not there, then open a upstream bug like https://github.com/intel/thermal_daemon/issues/275 "
     exit $RETVAL
 fi
_summary: Check if thermald is active
_description:
 Check if thermald is active
